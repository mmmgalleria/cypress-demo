const { defineConfig } = require("cypress");
const mysql = require("mysql");
const { Client } = require('pg');

function queryTestDb(query, config) {
  // creates a new mysql connection using credentials from cypress.json env's
  const connection = mysql.createConnection(config.env.db);
  // start connection to db
  connection.connect();
  // exec query + disconnect to db as a Promise
  return new Promise((resolve, reject) => {
    connection.query(query, (error, results) => {
      if (error) reject(error);
      else {
        connection.end();
        // console.log(results)
        return resolve(results);
      }
    });
  });
}

async function queryPostgres(query, config) {
  const client = new Client(config.env.pq)
  await client.connect()
  const res = await client.query(query)
  await client.end()
  return res.rows;
}

module.exports = defineConfig({
    "baseUrl" : "https://api.jsonbin.io",
    "fixturesFolder": "cypress/fixtures/sit",
    "video" : false,
    "env" : {
        "environment" : "sit",
        "headers" : {
            "secret-key" : "$2b$10$a5pPzFpI0FUDLGbUCBwc4./npTSp.U2nrULweVf8DJ28HCZWhqlse"
        }
    },
    "env":{
        "db": {
        "host": "fababean21s.se.scb.co.th",
        "user": "fe_robot",
        "password": "D5b1Zg^k^yM@oYk0!wR#",
        "port": 3310,
        },
        "pq": {
        "user": "postgres",
        "password": "",
        "host": "localhost",
        "database": "postgres",
        "ssl": false,
        "port": 5432
        }
    },
    e2e: {
        setupNodeEvents(on, config) {
        // implement node event listeners here
        on("task", {
            queryMysql: query => {
            return queryTestDb(query, config)
            },
            async queryPG(query){
            const client = new Client(config.env.pq)
            await client.connect()
            const res = await client.query(query)
            await client.end()
            return res.rows;
            },
            queryPostgres: query => {
            return queryPostgres(query, config)
        },
        })
        },
    },
});
