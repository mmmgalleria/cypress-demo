const commonData = require("../../fixtures/common/data.json")

describe('Variable', function () {
    // test case
   it('Constant', function() {
       const sum = 1 + 1;
       cy.log(sum)
       sum = 3     
    }) 

    it('Variable', function() {
        cy.log(varData)
        var varData = 2
        cy.log(varData)
        varData = 4
        cy.log(varData)
     })

     it('Let', function() {
        // cy.log(letData)
        let letData = 2
        cy.log(letData)
     })

     it('Aliases', function() {
        cy.fixture("example.json").then((loadData) => {
            cy.log(loadData)
            // console.log(loadData)
        })
     })

     it('Load Data by Environment', function() {
         cy.fixture("example.json").then((loadData) => {
            cy.log(loadData.env)
            cy.log(Cypress.env().environment)
            cy.log(commonData.key)
        })
     })

     it('Load Common Data', function() {
        cy.log(commonData)
    })
})