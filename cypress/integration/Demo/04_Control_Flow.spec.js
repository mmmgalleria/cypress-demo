const commonUtil = require("../../support/commonUtil.js")

describe('Control Flow', function () {
    // test case
   it('Conditional Testing', function() {
       const number = 5
       if( number > 5){
           cy.log("Number is greater than 5")
       } else if (number == 5){
            cy.log("Number is equal 5")
       } else {
        cy.log("Number is less than 5")
       }
    })

    it('Conditional Testing', function() {
        let number = 5
        if(number = 5){
             cy.log("Number is equal 5")
        }
     })

     it('Repeating', function() {
        for (let i = 0; i < 10; i++){
            cy.log(i)
        }
     })

     it('Nested Loop', function() {
        for (let i = 0; i < 4; i++){
            for (let j = 0; j < 4; j++){
                cy.log(i,j)
            }
        }
     })

     it('Data Driven', function() {
        let obj = {a:1, b:2, c:3}
        for (let i in obj) {
            cy.log(i, obj[i])
        }
     })

     it('Load Other Function', function() {
        cy.log(commonUtil.addNumber(3,4))
        cy.log(cy.addNumber(1,2))
     })

})