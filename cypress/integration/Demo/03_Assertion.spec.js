describe('Assertion', function () {
    // test case
   it('Assert String', function() {
       const expectedData = "Test String Data"
       const data = "Test String Data"
       expect(data).to.eq(expectedData)
       expect(data).to.equal(expectedData)
    })

    it('Assert equal JSON', function() {
        const expectedData = {
            "name": "Using fixtures to represent data",
            "email": "hello@cypress.io",
            "body": "Fixtures are a great way to mock data for responses to routes"
          }
        const data = {
            "name": "Using fixtures to represent data",
            "email": "hello@cypress.io",
            "body": "Fixtures are a great way to mock data for responses to routes"
          }
        expect(data).to.eq(expectedData)
     })

     it('Assert deep equal JSON', function() {
        const expectedData = {
            "name": "Using fixtures to represent data",
            "email": "hello@cypress.io",
            "body": "Fixtures are a great way to mock data for responses to routes"
          }
        const data = {
            "email": "hello@cypress.io",
            "name": "Using fixtures to represent data",
            "body": "Fixtures are a great way to mock data for responses to routes"
          }
        expect(data).to.deep.eq(expectedData)
     })

     it('Disorder Array', function() {
        const expectedData = [3,2,1]
        const data = [1,2,3,4,5]
        expect(data).to.include.members(expectedData)
     })

     it('Disorder Array', function() {
        const expectedData = [3,2,1,4,5]
        const data = [1,2,3]
        expect(expectedData).to.include.members(data)
     })

     it('Assert to include JSON', function() {
        const expectedData = {
            "email": "hello@cypress.io",
            "name": "Using fixtures to represent data"
          }
        const data = {
            "email": "hello@cypress.io",
            "name": "Using fixtures to represent data",
            "body": []
          }
        expect(data).to.include(expectedData)
     })

     it('Assert JSON Key', function() {
        const expectedData = ["email","name"]
        const data = {
            "email": "hello@cypress.io",
            "name": "Using fixtures to represent data",
            "body": []
          }
        expect(data).to.include.all.key(expectedData)
     })

     it('Assert JSON Key', function() {
        const expectedData = {
            "email": "hello@cypress.io",
            "name": "Using fixtures to represent data"
          }
        const data = {
            "email": "hello@cypress.io",
            "name": "Using fixtures to represent data",
            "body": []
          }
        expect(data).to.include.all.key(Object.keys(expectedData))
     })

})