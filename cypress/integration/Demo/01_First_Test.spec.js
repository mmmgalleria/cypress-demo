describe("My First Test Suite", function () {
  // test case
  it("First Test Case", function () {
    const sum = 1 + 1;
    cy.log(sum);
    console.log(sum);
    expect(2).to.equal(sum);
  });

  it("Test Case with Promise", function () {
    const sum = 1 + 1;
    cy.log(sum).then(() => {
      expect(2).to.equal(sum);
    });
  });
});
