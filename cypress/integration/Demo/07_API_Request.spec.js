const commonRequest = require("../../support/commonRequest.js")
const statusCode = require("../../fixtures/common/statusCode.json")
const commonPath = require("../../fixtures/common/commonPath.json")

describe('API Request', () => {

    it("Get Simple API", () => {
        cy.request({
            method : "GET",
            url : "https://jsonplaceholder.typicode.com/users"
        }).then(response => {
            expect(response.status).to.equal(200)
        })
    })

    it("POST API with Header", () => {
        const expected_response = {
            "name": "sue"
        }
        cy.request({
            method : "POST",
            url : "postman-api-learner.glitch.me/info",
            headers : {
                "Content-Type" : "application/json"
            },
            body : {
                "name": "sue"
            }
        }).then(response => {
            expect(response.status).to.equal(200)
            cy.log(response.body.data)
            expect(response.body.data).to.deep.eq(expected_response)
        })
    })

    it("API Lab 1", () => {
        cy.request({
            method : "GET",
            url : "/b/5eba7af347a2266b14772326",
            headers : Cypress.env().headers
        }).then(response => {
            expect(response.status).to.equal(200)
        })

        commonRequest.request_get("/b/5eba7af347a2266b14772326", Cypress.env().headers).then(response => {
                expect(response.status).to.equal(200)
        })

        commonRequest.request_get(commonPath.postRequestWithHeaders, Cypress.env().headers).then(response => {
            expect(response.status).to.equal(statusCode.httpStatusCode.success)
        })
    })

    it("API Lab 2", () => {
        commonRequest.request_get(commonPath.postRequestWithHeaders, Cypress.env().headers).then(response => {
            expect(response.status).to.equal(statusCode.httpStatusCode.success)
            cy.fixture("response.json").then(expected_data => {
                expect(response.body).to.deep.eq(expected_data.response)
            })
        })
    })

    it.only("API Lab 3", () => {
        // Data Driven
        let sumDataDriven = 0
        cy.fixture("response.json").then(data => {
            const response = data.response
            for (let i in response){
                let friend = response[i].friends
                for (let j in friend){
                    sumDataDriven = sumDataDriven + friend[j].balance
                }
            }
            cy.log(sumDataDriven)
        })
        // For Loop
        let sumForLoop = 0
        cy.fixture("response.json").then(data => {
            const response = data.response
            for (let i = 0; i < data.response.length; i++){
                let friend = response[i].friends
                for (let j = 0; j < friend.length; j++){
                    sumForLoop = sumForLoop + friend[j].balance
                }
            }
            cy.log(sumForLoop)
        })
    })
})