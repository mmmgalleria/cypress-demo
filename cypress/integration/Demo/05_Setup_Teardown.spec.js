describe('Setup Teardown', () => {

    before(() => {
        cy.log("Test Suite Set Up")
    })

    // after(() => {
    //     cy.log("Test Suite Tear Down")
    // })

    beforeEach(() => {
        cy.log("Test Case Set Up")
    })

    // afterEach(() => {
    //     cy.log("Test Case Tear Down")
    // })

    describe('Setup Teardown Group1', () => {

        after(() => {
            cy.log("Test Suite Tear Down")
        })
    
        it("Testcase 1", () => {
            cy.log("Testcase 1")
        })
    
        it("Testcase 2", () => {
            cy.log("Testcase 2")
        })
    })

    describe('Setup Teardown Group2', () => {

        after(() => {
            cy.log("Test Suite Tear Down")
        })
    
        it("Testcase 3", () => {
            cy.log("Testcase 3")
        })
    
        it("Testcase 4", () => {
            cy.log("Testcase 4")
        })
    })

    it("Testcase 1", () => {
        cy.log("Testcase 1")
    })

    it("Testcase 2", () => {
        cy.log("Testcase 2")
    })

    it("Testcase 3", () => {
        cy.log("Testcase 3")
    })
})